import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('src/modules/age-of-empires/src/public-api').then((m) => m.AgeOfEmpiresModule),
    canActivate: []
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),StoreModule.forRoot({}), ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
