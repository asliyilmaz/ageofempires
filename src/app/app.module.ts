import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AgeofempiresEffects, AgeOfEmpiresModule } from 'src/modules/age-of-empires/src/public-api';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    FormsModule,
    AgeOfEmpiresModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    EffectsModule.forRoot([AgeofempiresEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
