import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AgeOfEmpiresComponent } from './age-of-empires.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromAgeofempires from './+state/ageofempires.reducer';
import { AgeofempiresEffects } from './+state/ageofempires.effects';
import { AgeOfDashboardComponent } from './components/age-of-dashboard/age-of-dashboard.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { UnitsPageComponent } from './components/units-page/units-page.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSliderModule } from '@angular/material/slider';
import { MatTableModule } from '@angular/material/table';
import { Ng5SliderModule } from 'ng5-slider';
import { FilteredDataTableComponent } from './components/filtered-data-table/filtered-data-table.component';

@NgModule({
  declarations: [
    AgeOfEmpiresComponent,
    AgeOfDashboardComponent,
    TopBarComponent,
    UnitsPageComponent,
    FilteredDataTableComponent
  ],
  imports: [
    MatCheckboxModule,
    MatSliderModule,
    Ng5SliderModule,
    MatTableModule,
    CommonModule,
    StoreModule.forFeature(fromAgeofempires.AGEOFEMPIRES_FEATURE_KEY, fromAgeofempires.reducer),
    EffectsModule.forFeature([AgeofempiresEffects]),
    RouterModule.forChild([
      { path: '', pathMatch: 'full', component: AgeOfDashboardComponent },
      { path: 'units-page', pathMatch: 'full', component: UnitsPageComponent },
      { path: 'table-details', pathMatch: 'full', component: FilteredDataTableComponent }
    ])
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  exports: [
    AgeOfEmpiresComponent,
    TopBarComponent
  ]
})
export class AgeOfEmpiresModule { }
