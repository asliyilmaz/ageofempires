import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { DataPersistence, fetch } from '@nrwl/angular';
import { map, switchMap } from 'rxjs/operators';

import * as AgeofempiresFeature from './ageofempires.reducer';
import * as AgeofempiresActions from './ageofempires.actions';
import { AgeOfEmpiresService } from '../age-of-empires.service';

@Injectable()
export class AgeofempiresEffects {
  init$ = createEffect(() => this.actions$.pipe(
    ofType(AgeofempiresActions.init),
    fetch({
      run: action => {
        // Your custom service 'load' logic goes here. For now just return a success action...
        return AgeofempiresActions.loadAgeofempiresSuccess({ ageofempires: [] });
      },

      onError: (action, error) => {
        console.error('Error', error);
        return AgeofempiresActions.loadAgeofempiresFailure({ error });
      }
    }))
  );
  GetAgeOfEmpiresData$ = createEffect(() =>
  this.actions$.pipe(
    ofType(AgeofempiresActions.GetAgeOfEmpiresData),
    switchMap((action: ReturnType<typeof AgeofempiresActions.GetAgeOfEmpiresData>) => {
      return this.dataService.data$.pipe(
      map((res: any) => {
          return AgeofempiresActions.GetAgeOfEmpiresDataLoaded({ payload: res.units });
        })
      );
  })

  )
);

  constructor(
    private actions$: Actions,
    private dataService: AgeOfEmpiresService,

  ) { }
}
