import { createAction, props } from '@ngrx/store';
import { AgeofempiresEntity } from './ageofempires.models';

export const init = createAction(
  '[Ageofempires Page] Init'
);

export const loadAgeofempiresSuccess = createAction(
  '[Ageofempires/API] Load Ageofempires Success',
  props<{ ageofempires: AgeofempiresEntity[] }>()
);

export const loadAgeofempiresFailure = createAction(
  '[Ageofempires/API] Load Ageofempires Failure',
  props<{ error: any }>()
);

export const GetAgeOfEmpiresData = createAction(
  '[Ageofempires/API] Get Age Of Empires Data'
);

export const GetAgeOfEmpiresDataLoaded = createAction(
  '[Ageofempires/API] Get Age Of Empires Data Loaded',
  props<{ payload: any }>()
);
export const GetAgeOfEmpiresFilterData = createAction(
  '[Ageofempires/API] Get Age Of Empires Filter Data',
  props<{ payload: any }>()
);
export const GetAgeOfEmpiresDetailTableValue = createAction(
  '[Ageofempires/API] Get Age Of Empires Detail Table Value',
  props<{ payload: any }>()
);
export const GetAgeOfEmpiresSliderFilterValue = createAction(
  '[Ageofempires/API] Get Age Of Empires Slider Filter Value',
  props<{ payload: any }>()
);