import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as AgeofempiresActions from './ageofempires.actions';

import { AgeofempiresEntity } from './ageofempires.models';

export const AGEOFEMPIRES_FEATURE_KEY = 'ageofempires';

export interface AgeOfEmpiresState extends EntityState<AgeofempiresEntity> {
  selectedId?: string | number; // which Ageofempires record has been selected
  loaded: boolean; // has the Ageofempires list been loaded
  error?: string | null; // last known error (if any)
  data?: any;
  newData?: any;
  filteredData?: any;
  sliderFilterData?: any;

}

export interface AgeofempiresPartialState {
  readonly [AGEOFEMPIRES_FEATURE_KEY]: AgeOfEmpiresState;
}

export const ageofempiresAdapter: EntityAdapter<AgeofempiresEntity> = createEntityAdapter<AgeofempiresEntity>();

export const initialState: AgeOfEmpiresState = ageofempiresAdapter.getInitialState({
  // set initial required properties
  loaded: false
});

const ageofempiresReducer = createReducer(
  initialState,
  on(AgeofempiresActions.init,
    state => ({ ...state, loaded: false, error: null })
  ),
  on(AgeofempiresActions.loadAgeofempiresSuccess,
    (state, { ageofempires }) => ageofempiresAdapter.setAll(ageofempires, { ...state, loaded: true })
  ),
  on(AgeofempiresActions.loadAgeofempiresFailure,
    (state, { error }) => ({ ...state, error })
  ),
  on(AgeofempiresActions.GetAgeOfEmpiresDataLoaded, (state, action) => {
    return {
      ...state,
      data: action.payload,
      newData: action.payload
    };
  }),
  on(AgeofempiresActions.GetAgeOfEmpiresFilterData, (state, action) => {
    let mod = action.payload;
    const newData = state.data.filter((element: any) => {
      return mod == element.age;
    });
    if (mod == 'all') {
      return {
        ...state,
        newData: state.data
      }
    } else {
      return {
        ...state,
        newData: newData
        
      }
    }

  }),
  on(AgeofempiresActions.GetAgeOfEmpiresDetailTableValue, (state, action) => {
    let id = action.payload;
    const filteredData = state.data.filter((element: any) => {
      return id == element.id;
    });
    return {
      ...state,
      filteredData: filteredData
    };
  }),
  on(AgeofempiresActions.GetAgeOfEmpiresSliderFilterValue, (state, action) => {
    let filterSlidered;
    if(state.sliderFilterData){
      filterSlidered = state.sliderFilterData.filter((element: any) => {
        if (element.cost && element.cost[action.payload.item]) {
          return element.cost[action.payload.item] < action.payload.highValue && action.payload.value < element.cost[action.payload.item];
        } else { return }
  
      });
    }else{
      filterSlidered = state.data.filter((element: any) => {
        if (element.cost && element.cost[action.payload.item]) {
          return element.cost[action.payload.item] < action.payload.highValue && action.payload.value < element.cost[action.payload.item];
        } else { return }
  
      });
    }
 

      return {
        ...state,
        sliderFilterData: filterSlidered
      };
   
  })
);

export function reducer(state: AgeOfEmpiresState | undefined, action: Action) {
  return ageofempiresReducer(state, action);
}
