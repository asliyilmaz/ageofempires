/**
 * Interface for the 'Ageofempires' data
 */
export interface AgeofempiresEntity {
  id: string | number;  // Primary ID
  name: string;
};