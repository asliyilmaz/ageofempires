import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AGEOFEMPIRES_FEATURE_KEY, AgeOfEmpiresState, ageofempiresAdapter } from './ageofempires.reducer';

// Lookup the 'Ageofempires' feature state managed by NgRx
export const getAgeofempiresState = createFeatureSelector<AgeOfEmpiresState>(AGEOFEMPIRES_FEATURE_KEY);

const { selectAll, selectEntities } = ageofempiresAdapter.getSelectors();

export const getAgeofempiresLoaded = createSelector(
  getAgeofempiresState,
  (state: AgeOfEmpiresState) => state.loaded
);

export const getAgeofempiresError = createSelector(
  getAgeofempiresState,
  (state: AgeOfEmpiresState) => state.error
);

export const getAllAgeofempires = createSelector(
  getAgeofempiresState,
  (state: AgeOfEmpiresState) => selectAll(state)
);

export const getAgeofempiresEntities = createSelector(
  getAgeofempiresState,
  (state: AgeOfEmpiresState) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getAgeofempiresState,
  (state: AgeOfEmpiresState) => state.selectedId
);

export const getSelected = createSelector(
  getAgeofempiresEntities,
  getSelectedId,
  (entities, selectedId) => (selectedId ? entities[selectedId] : undefined)
);
export const getData = createSelector(
  getAgeofempiresState, 
  (state: AgeOfEmpiresState) => state.newData
);
export const getFilteredData = createSelector(
  getAgeofempiresState, 
  (state: AgeOfEmpiresState) => state.filteredData
);
export const getSliderFilterData= createSelector(
  getAgeofempiresState, 
  (state: AgeOfEmpiresState) => state.sliderFilterData
);

