import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgeOfDashboardComponent } from './age-of-dashboard.component';

describe('AgeOfDashboardComponent', () => {
  let component: AgeOfDashboardComponent;
  let fixture: ComponentFixture<AgeOfDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgeOfDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgeOfDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
