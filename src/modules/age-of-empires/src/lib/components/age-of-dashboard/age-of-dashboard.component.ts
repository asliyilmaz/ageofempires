import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-age-of-dashboard',
  templateUrl: './age-of-dashboard.component.html',
  styleUrls: ['./age-of-dashboard.component.scss']
})
export class AgeOfDashboardComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  onClick(){
    this.router.navigate(['/units-page']);
  }
}
