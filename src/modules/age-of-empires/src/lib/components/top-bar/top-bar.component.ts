import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  @Output() clickMenuElement = new EventEmitter();

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  onClickMenu(event: any){
    if(event == 'Units'){
      this.router.navigate(['/units-page']);
    }else{
      this.router.navigate(['/home']);
    }
  }
}
