import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilteredDataTableComponent } from './filtered-data-table.component';

describe('FilteredDataTableComponent', () => {
  let component: FilteredDataTableComponent;
  let fixture: ComponentFixture<FilteredDataTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilteredDataTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteredDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
