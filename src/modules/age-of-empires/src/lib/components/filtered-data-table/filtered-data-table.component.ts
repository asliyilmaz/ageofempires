import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AgeOfEmpiresState } from '../../+state/ageofempires.reducer';
import { getFilteredData } from '../../+state/ageofempires.selectors';

@Component({
  selector: 'app-filtered-data-table',
  templateUrl: './filtered-data-table.component.html',
  styleUrls: ['./filtered-data-table.component.scss']
})
export class FilteredDataTableComponent implements OnInit {
  filterTableData$!: Observable<any>;
  titles$!: Observable<any>;

  constructor(private store: Store<AgeOfEmpiresState>) { }
  
  ngOnInit(): void {
    this.filterTableData$=this.store.pipe(select(getFilteredData));
    this.titles$=this.filterTableData$.pipe(map((el)=>{
      return el.reduce((acc:any,key:any)=>{
        return [
          ...acc,
          ...Object.keys(key)
        ];
      },[])
    })
    );
  }

}
