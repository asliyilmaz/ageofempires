import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Options } from 'ng5-slider';
import { combineLatest, Observable } from 'rxjs';
import { count, filter, map, startWith } from 'rxjs/operators';
import { GetAgeOfEmpiresData, GetAgeOfEmpiresDetailTableValue, GetAgeOfEmpiresFilterData, GetAgeOfEmpiresSliderFilterValue } from '../../+state/ageofempires.actions';
import { AgeOfEmpiresState } from '../../+state/ageofempires.reducer';
import { getData, getSliderFilterData } from '../../+state/ageofempires.selectors';


@Component({
  selector: 'app-units-page',
  templateUrl: './units-page.component.html',
  styleUrls: ['./units-page.component.scss']
})
export class UnitsPageComponent implements OnInit {
  val: any;
  combinedArray: any;
  filteredThreshold: any;
  costList: string[] = ['Food', 'Wood', 'Gold'];
  displayedColumns: string[] = ['id', 'name', 'age', 'costs'];
  tableData$!: Observable<any>;
  filteredSliderData$!: Observable<any>;
  combineData$!: Observable<any>;
  foodHighValue: number = 200;
  woodHighValue: number = 200;
  goldHighValue: number = 200;
  goldOptions: Options = {
    floor: 0,
    ceil: 200,
    disabled: true
  };
  foodOptions: Options = {
    floor: 0,
    ceil: 200,
    disabled: true
  };
  woodOptions: Options = {
    floor: 0,
    ceil: 200,
    disabled: true
  };
  checked = false;
  foodValue = 0;
  woodValue = 0;
  goldValue = 0;
  vertical = false;

  constructor(private store: Store<AgeOfEmpiresState>, private router: Router) { }
  ngOnInit(): void {
    this.store.dispatch(GetAgeOfEmpiresData());


    this.tableData$ = this.store.pipe(select(getData));
    this.filteredSliderData$ = this.store.pipe(select(getSliderFilterData));
    this.combineData$ = combineLatest([
      this.tableData$,
      this.filteredSliderData$
    ]).pipe(
      filter(([tableData]) => !!tableData),
      map(([tableData, filteredSliderData]) => {
        let food: any, wood, gold: any;
        let duplicates: any;
        let sonuc= [];


        if (filteredSliderData && tableData) {
          if (filteredSliderData.length > 0) {
            return filteredSliderData.filter((x: any) => tableData.includes(x));
          } else {

            food = tableData.filter((element: any) => {
              if (element.cost && element.cost.Food) {
                return element.cost.Food < this.foodHighValue && this.foodValue < element.cost.Food && !this.foodOptions.disabled;
              } else { return }

            });
            wood = tableData.filter((element: any) => {
              if (element.cost && element.cost.Wood) {
                return element.cost.Wood < this.woodHighValue && this.woodValue < element.cost.Wood && !this.woodOptions.disabled;
              } else { return }


            });
            gold = tableData.filter((element: any) => {
              if (element.cost && element.cost.Gold) {
                return element.cost.Gold < this.goldHighValue && this.goldValue < element.cost.Gold && !this.goldOptions.disabled;
              } else { return }

            });
            const data = [...wood, ...gold, ...food];
            duplicates = [...data];
            const yourArrayWithoutDuplicates = [...new Set(duplicates)]

            yourArrayWithoutDuplicates.forEach((item) => {
              const i = duplicates.indexOf(item)
              duplicates = duplicates
                .slice(0, i)
                .concat(duplicates.slice(i + 1, duplicates.length))
            });
          }
          return duplicates;
        } else {
          return tableData;
        }

      })
    );

  }
  handleCheckBox(event: any) {
    if (event.item == 'Gold') {
      this.goldOptions = Object.assign({}, this.goldOptions, { disabled: !event.checked });
      if (!event.checked) {
        this.goldValue = 0;
        this.goldHighValue = 200;
      }

    } else if (event.item == 'Wood') {
      this.woodOptions = Object.assign({}, this.woodOptions, { disabled: !event.checked });
      if (!event.checked) {
        this.woodValue = 0;
        this.woodHighValue = 200;
      }

    } else if (event.item == 'Food') {
      this.foodOptions = Object.assign({}, this.foodOptions, { disabled: !event.checked });
      if (!event.checked) {
        this.foodValue = 0;
        this.foodHighValue = 200;
      }

    }
  }
  handleClickThresholdValue(event: any) {

    this.store.dispatch(GetAgeOfEmpiresSliderFilterValue({ payload: event }));

  }
  tableSelected(event: any) {
    this.store.dispatch(GetAgeOfEmpiresFilterData({ payload: event }));

  }
  tableCellClick(event: any) {
    this.store.dispatch(GetAgeOfEmpiresDetailTableValue({ payload: event }));
    this.router.navigate(['/table-details']);
  }
}
